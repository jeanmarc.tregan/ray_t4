/* Copyright (C) 2017-2019 Université Paul Sabatier
 *  (tregan@laplace.univ-tlse.fr)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
  FILE* fichier = NULL;
  FILE* obj = NULL;
  /*int nLignes = compte(fichier);*/
  char buf[512];
  int i=0;
  int nombre_point=0;
  int offset = 0;
  double x1=0, x2=0, x3=0;

  /* Choix des rayons à traiter */
    fichier = fopen("diffusion.dat", "r");
    obj = fopen("rayons.obj", "w");
  
  while (fgets(buf, sizeof(buf)-1, fichier)) /* -1 pour caractère particulier */
    {	
      if(sscanf(buf, "%lf %lf %lf", &x1, &x2, &x3) == 3) 
	{
	  fprintf(obj, "v %f %f %f\n", x1, x2, x3);
	  nombre_point++;
	} else { /* Ligne vide */
	for (i=0; i < nombre_point-1; i++)
	  {
	    fprintf(obj, "l %d %d\n ", offset+i+1, offset+i+2);
	  }
	fprintf(obj, "\n");
	offset += nombre_point;
	nombre_point = 0;
      }
    }
	
  fclose(fichier);
  return 0;
	
}


