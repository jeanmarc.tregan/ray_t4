/* Copyright (C)
 *    2015-2019 |Meso|Star> (vincent.forest@meso-star.com)
 *    2017-2019 Université Paul Sabatier (tregan@laplace.univ-tlse.fr)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <rsys/stretchy_array.h>
#include <rsys/float3.h>

#include <star/s3d.h>
#include <star/ssp.h>
#include <star/smc.h>

#include "s4vs_realization.h"

#define FP_TO_METER 1.f/1000.f
#define BOLTZMANN_CONSTANT 5.6696e-8 /* W/m^2/K^4 */
#define T_env 600.0
#define T_hat T_env
#define T_hat2 (T_hat*T_hat)
#define T_hat3 (T_hat*T_hat2)

enum vertex_status {
  TEMPERATURE_DONE,
  TEMPERATURE_BOUNDARY,
  TEMPERATURE_SOLID
};

struct vertex {
  float pos[3];
  double time;
  double temperature;
  struct s3d_hit hit; /* Hit of the vertex */
};

static const struct vertex VERTEX_NULL = {{0,0,0},0,0,S3D_HIT_NULL__};

static res_T
conduction
  (struct s4vs_context* ctx,
   struct vertex* vtx,
   struct ssp_rng* rng);

static res_T
boundary
  (struct s4vs_context* ctx,
   struct vertex* vtx,
   struct ssp_rng* rng);

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static INLINE void
write_path(FILE* stream, const float* vertices, const size_t offset)
{
  size_t nvertices = sa_size(vertices) / 3;
  size_t i;
  if(nvertices < 2) return;
  FOR_EACH(i, 0, nvertices) {
    fprintf(stream, "v %g %g %g\n",
      vertices[i*3+0],
      vertices[i*3+1],
      vertices[i*3+2]);
  }
  FOR_EACH(i, 1, nvertices) {
    fprintf(stream, "l %lu %lu\n",
      (unsigned long)(i+offset),
      (unsigned long)(i+offset+1));
  }
}

int
s4vs_discard_self_hit
  (const struct s3d_hit* hit,
   const float ray_org[3],
   const float ray_dir[3],
   void* ray_data,
   void* filter_data)
{
  const struct s3d_primitive* prim_from = ray_data;
  /* Avoid unused variable warn */
  (void)ray_org, (void)ray_dir, (void)filter_data;
  return prim_from ? S3D_PRIMITIVE_EQ(prim_from, &hit->prim) : 0;
}

static double
initial_condition(const struct s4vs_context* ctx, const float x[3])
{
  (void)x, (void)ctx;
  return 200;
}

res_T
conduction(struct s4vs_context* ctx, struct vertex* vtx, struct ssp_rng* rng)
{
  ASSERT(ctx && vtx && rng);

  for(;;) {
    struct s3d_primitive prim = S3D_PRIMITIVE_NULL;
    const float range[2] = {0, FLT_MAX};
    float delta = ctx->delta;
    struct s3d_hit hit0, hit1;
    float u0[3], u1[3];
    double tau, mu;

    /* On tire une direction */
    ssp_ran_sphere_uniform_float(rng, u0, NULL);
    f3_minus(u1, u0);

    S3D(scene_view_trace_ray(ctx->view, vtx->pos, u0, range, &prim, &hit0));
    S3D(scene_view_trace_ray(ctx->view, vtx->pos, u1, range, &prim, &hit1));
    if(S3D_HIT_NONE(&hit0) || S3D_HIT_NONE(&hit1)) {
      printf("conduction\n");
      return RES_UNKNOWN_ERR;
    }

    delta = MMIN(delta, hit0.distance);
    delta = MMIN(delta, hit1.distance);

    mu = (6*ctx->lambda) / (ctx->rho*ctx->cp*delta*FP_TO_METER*delta*FP_TO_METER);
    tau = ssp_ran_exp(rng, mu);
    vtx->time -= tau;
    if(vtx->time <= 0) { /* Condition initiale */
      vtx->temperature = initial_condition(ctx, vtx->pos);
      return RES_OK;
    } else {
      vtx->pos[0] += u0[0]*delta;
      vtx->pos[1] += u0[1]*delta;
      vtx->pos[2] += u0[2]*delta;
    }

    if(delta == hit0.distance) {
      vtx->hit = hit0;
      return boundary(ctx, vtx, rng);
    }
  }

  return RES_OK;
}

res_T
boundary
  (struct s4vs_context* ctx,
   struct vertex* vtx,
   struct ssp_rng* rng)
{
  const float range[2] = {0, FLT_MAX};
  float normal[3];
  struct s3d_hit hit;
  float delta = ctx->delta;
  double tmp;
  double p_C;
  double p_F;
  double p_R;
  double r;
  double mu;
  double tau;
  res_T res = RES_OK;
  ASSERT(ctx && vtx && rng && !S3D_HIT_NONE(&vtx->hit));
  #define CALL(Func) {if(RES_OK != (res = Func)) { return res; }}(void)0

  f3_normalize(normal, vtx->hit.normal);
  S3D(scene_view_trace_ray
    (ctx->view, vtx->pos, normal, range, &vtx->hit.prim, &hit));
  if(S3D_HIT_NONE(&hit)) return RES_UNKNOWN_ERR;

  if(hit.distance < delta) {
    delta = hit.distance * 0.5f;
  }

  /* Calcul des probabilités */
  {
    static int i = 0;
    tmp = (4.0*delta*FP_TO_METER*BOLTZMANN_CONSTANT*T_hat3+ctx->lambda);
    p_R = (BOLTZMANN_CONSTANT*delta*FP_TO_METER*T_hat3) / tmp;
    p_C = ctx->lambda / tmp;
    p_F = 0;

    if(!i) {
      printf("%g %g %g\n", p_C, p_R, 1-(p_R + p_C));
      ++i;
    }
  }

  /*printf("%g %g %g\n", p_R, p_C, 1-(p_R + p_C));
  exit(0);*/

  r = ssp_rng_canonical(rng);
  if(r < p_F) {
    FATAL("Fluids are not supported\n");
  } else if(r < p_F + p_C) { /* Conduction */
    mu = (6*ctx->lambda) / (ctx->rho*ctx->cp*delta*FP_TO_METER*delta*FP_TO_METER);
    tau = ssp_ran_exp(rng, mu);
    vtx->hit = S3D_HIT_NULL;
    vtx->time -= tau;
    if(vtx->time <= 0) { /* Condition initiale */
      vtx->temperature = initial_condition(ctx, vtx->pos);
    } else {
      vtx->pos[0] += normal[0]*delta;
      vtx->pos[1] += normal[1]*delta;
      vtx->pos[2] += normal[2]*delta;
      return conduction(ctx, vtx, rng);
    }
  } else if(r < p_F + p_C + p_R) { /* Radiative */
    float neg_normal[3], u[3];
    neg_normal[0] = -normal[0];
    neg_normal[1] = -normal[1];
    neg_normal[2] = -normal[2];

    ssp_ran_hemisphere_cos_float(rng, neg_normal, u, NULL);
    S3D(scene_view_trace_ray
      (ctx->view, vtx->pos, u, range, &vtx->hit.prim, &hit));
    if(S3D_HIT_NONE(&hit)) {
      /*const double T = u[0] < 0 ? 255.4205 : 337.1273;*/
      /*const double T = u[0] < 0 ? 290 : 310;*/
      const double T = T_env;
      vtx->temperature = (T*T*T*T) / T_hat3;
    } else {
      struct vertex v0, v1, v2, v3;
      v0 = v1 = v2 = v3 = *vtx;

      vtx->pos[0] += u[0]*hit.distance;
      vtx->pos[1] += u[1]*hit.distance;
      vtx->pos[2] += u[2]*hit.distance;
      vtx->hit = hit;

      CALL(boundary(ctx, &v0, rng));
      CALL(boundary(ctx, &v1, rng));
      CALL(boundary(ctx, &v2, rng));
      CALL(boundary(ctx, &v3, rng));

      vtx->temperature =
        v0.temperature
      * v1.temperature
      * v2.temperature
      * v3.temperature
      / T_hat3;
    }
  } else { /* Branching */
    struct vertex w0, w1, w2, w3;
    w0 = w1 = w2 = w3 = *vtx;

    CALL(boundary(ctx, &w0, rng));
    r = ssp_rng_canonical(rng);
    ASSERT(w0.temperature / T_hat <= 1.0);
    if(r < w0.temperature / T_hat) {
      vtx->temperature = T_hat;
      return RES_OK;
    }

    CALL(boundary(ctx, &w1, rng));
    r = ssp_rng_canonical(rng);
    ASSERT(w1.temperature / (3*T_hat) <= 1.0);
    if(r < w1.temperature / (3.0*T_hat)) {
      vtx->temperature = T_hat;
      return RES_OK;
    } else if(r < 1.0/3.0) {
      vtx->temperature = 0;
      return RES_OK;
    }

    CALL(boundary(ctx, &w2, rng));
    r = ssp_rng_canonical(rng);
    ASSERT((w1.temperature * w2.temperature) / (2*T_hat2) <= 1.0);
    if(r < (w1.temperature * w2.temperature) / (2*T_hat2)) {
      vtx->temperature = T_hat;
      return RES_OK;
    } else if(r < 0.5) {
      vtx->temperature = 0;
      return RES_OK;
    }

    CALL(boundary(ctx, &w3, rng));
    vtx->temperature =
      w1.temperature
    * w2.temperature
    * w3.temperature
    / T_hat2;
    return RES_OK;
  }
  #undef CALL
  return RES_OK;
}

/*******************************************************************************
 * Realization
 ******************************************************************************/
res_T
s4vs_realization
  (void* out_temperature,
   struct ssp_rng* rng,
   const unsigned ithread,
   void* context)
{
  struct s4vs_context* ctx = (struct s4vs_context*)context;
  struct vertex vtx = VERTEX_NULL;
  res_T res = RES_OK;
  (void)ithread;

  vtx.pos[0] = (float)ctx->x0[0];
  vtx.pos[1] = (float)ctx->x0[1];
  vtx.pos[2] = (float)ctx->x0[2];
  vtx.time = ctx->t_obs;

  res = conduction(ctx, &vtx, rng);
  if(res != RES_OK) return res;

  SMC_DOUBLE(out_temperature) = vtx.temperature;
  return RES_OK;
}

