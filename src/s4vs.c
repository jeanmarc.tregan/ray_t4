/* Copyright (C)
 *    2015-2019 |Meso|Star> (vincent.forest@meso-star.com)
 *    2017-2019 Université Paul Sabatier (tregan@laplace.univ-tlse.fr)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <rsys/cstr.h>
#include <rsys/float3.h>
#include <rsys/double3.h>
#include <rsys/math.h>
#include <rsys/mem_allocator.h>

#include <star/s3d.h>
#include <star/s3dut.h>
#include <star/s3daw.h>
#include <star/smc.h>

#include "s4vs_realization.h"

/*0 pour calculer l'observale, 1 pour construire l'obj du chemin*/
#define DUMP_PATH 0

static res_T
compute_4v_s(struct s3d_scene* scene, const size_t max_steps, const double ks)
{
  struct s4vs_context ctx;
  struct s3d_scene_view* view = NULL;
  struct smc_device* smc = NULL;
  struct smc_integrator integrator;
  struct smc_estimator* estimator = NULL;
  struct smc_estimator_status estimator_status;
  float lower[3];
  float upper[3];
  float sz[3];
  res_T res = RES_OK;
  ASSERT(scene && max_steps > 0 && ks > 0);

  S3D(scene_view_create(scene, S3D_SAMPLE|S3D_TRACE, &view));
  S3D(scene_view_get_aabb(view, lower, upper));
  sz[0] = upper[0] - lower[0];
  sz[1] = upper[1] - lower[1];
  sz[2] = upper[2] - lower[2];

  /* Initialize context for MC computation */
  ctx.view = view;
  ctx.ks = ks;
  ctx.g = PI/4.0;
  ctx.t_obs=0.05;
  d3(ctx.x0, 0.5, 0.5, 0.5); /* Bunny obj */
  /*d3(ctx.x0, 0, 0, 0);*/
  d3(ctx.w,  1, 1, 1);
  ctx.M=100;
  ctx.lambda=10;
  ctx.dump_path = DUMP_PATH;
  ctx.offset = 0;
  ctx.h=10.0;
  ctx.Tf=300;
  ctx.rho = 1.0;
  ctx.cp = 1.0;
  ctx.delta = MMIN(MMIN(sz[0], sz[1]), sz[2]) / 20.0f;
  printf("Delta = %g\n", ctx.delta);

  /* Setup Star-MC */
  if(DUMP_PATH) {
    SMC(device_create(NULL, NULL, 1, NULL, &smc));
  } else {
    SMC(device_create(NULL, NULL, SMC_NTHREADS_DEFAULT, NULL, &smc));
  }
  integrator.integrand = &s4vs_realization; /* Realization function */
  integrator.type = &smc_double; /* Type of the Monte Carlo weight */
  integrator.max_realisations = max_steps; /* Realization count */
  integrator.max_failures = max_steps/* / 1000*/; /* cancel if 0.1% of the realization fail */

  /* Solve */
  SMC(solve(smc, &integrator, &ctx, &estimator));

  /* Print the simulation results */
  SMC(estimator_get_status(estimator, &estimator_status));

  if(estimator_status.NF > integrator.max_failures) {
    fprintf(stderr,
	    "Too many failures (%lu). The scene might not match the prerequisites:\n"
	    "it must be closed and its normals must point *into* the volume.\n",
	    (unsigned long)estimator_status.NF);
    goto error;
  }
  if(!ctx.dump_path) {
    printf("T = %g +/- %g [%g, %g]; # failures: %lu/%lu\n",
      SMC_DOUBLE(estimator_status.E),
      SMC_DOUBLE(estimator_status.SE),
      SMC_DOUBLE(estimator_status.E) - 3*SMC_DOUBLE(estimator_status.SE),
      SMC_DOUBLE(estimator_status.E) + 3*SMC_DOUBLE(estimator_status.SE),
      (unsigned long)estimator_status.NF,
      (unsigned long)max_steps);

    /*printf("T theo = %g\n",
     ctx.M*sin(ctx.w[0]*ctx.x0[0])*sin(ctx.w[1]*ctx.x0[1])*sin(ctx.w[2]*ctx.x0[2])*exp(-ctx.lambda*(pow(ctx.w[0],2)+pow(ctx.w[1],2)+pow(ctx.w[2],2))*ctx.t_obs));*/
  }
exit:
  /* Clean-up data */
  if(view) S3D(scene_view_ref_put(view));
  if(smc) SMC(device_ref_put(smc));
  if(estimator) SMC(estimator_ref_put(estimator));
  return res;

error:
  goto exit;
}

/* Create a S3D scene from an obj in a scene */
static res_T
import_obj(const char* filename, struct s3d_scene** out_scene)
{
  struct s3d_device* s3d = NULL;
  struct s3d_scene* scene = NULL;
  struct s3daw* s3daw = NULL;
  const int VERBOSE = 0;
  size_t ishape, nshapes;
  res_T res = RES_OK;
  ASSERT(out_scene);

  #define CALL(Func) if(RES_OK != (res = Func)) goto error
  CALL(s3d_device_create(NULL, NULL, VERBOSE, &s3d));
  CALL(s3daw_create(NULL, NULL, NULL, NULL, s3d, VERBOSE, &s3daw));
  CALL(s3daw_load(s3daw, filename));
  CALL(s3daw_get_shapes_count(s3daw, &nshapes));
  CALL(s3d_scene_create(s3d, &scene));
  FOR_EACH(ishape, 0, nshapes) {
    struct s3d_shape* shape;
    CALL(s3daw_get_shape(s3daw, ishape, &shape));
    CALL(s3d_mesh_set_hit_filter_function(shape, s4vs_discard_self_hit, NULL));
    CALL(s3d_scene_attach_shape(scene, shape));
  }
  #undef CALL

exit:
  /* Release memory */
  if(s3daw) S3DAW(ref_put(s3daw));
  if(s3d) S3D(device_ref_put(s3d));
  *out_scene = scene;
  return res;
error:
  if(scene) {
    S3D(scene_ref_put(scene));
    scene = NULL;
  }
  goto exit;
}

static void
mesh_get_position(const unsigned ivert, float pos[3], void* ctx)
{
   struct s3dut_mesh_data* mdata = ctx;
   pos[0] = (float)mdata->positions[ivert*3 + 0];
   pos[1] = (float)mdata->positions[ivert*3 + 1];
   pos[2] = (float)mdata->positions[ivert*3 + 2];
}

static void
mesh_get_indices(const unsigned itri, unsigned ids[3], void* ctx)
{
   struct s3dut_mesh_data* mdata = ctx;
   ids[0] = (unsigned)mdata->indices[itri*3 + 0];
   ids[1] = (unsigned)mdata->indices[itri*3 + 1];
   ids[2] = (unsigned)mdata->indices[itri*3 + 2];
}

static INLINE void
create_sphere(struct s3d_scene** out_scene)
{
  struct s3d_device* s3d;
  struct s3d_shape* shape;
  struct s3d_scene* scene;
  struct s3d_vertex_data attrib[1];
  struct s3dut_mesh* mesh;
  struct s3dut_mesh_data mdata;

  S3DUT(create_sphere(NULL,0.5, 64, 32, &mesh));
  S3DUT(mesh_get_data(mesh, &mdata));

  attrib[0].usage = S3D_POSITION;
  attrib[0].type = S3D_FLOAT3;
  attrib[0].get = mesh_get_position;

  S3D(device_create(NULL, NULL, 0, &s3d));
  S3D(scene_create(s3d, &scene));
  S3D(shape_create_mesh(s3d, &shape));
  S3D(mesh_setup_indexed_vertices
    (shape,
     (unsigned)mdata.nprimitives,
     mesh_get_indices,
     (unsigned)mdata.nvertices,
     attrib,
     1,
     &mdata));

  S3D(mesh_set_hit_filter_function(shape, s4vs_discard_self_hit, NULL));
  S3D(scene_attach_shape(scene, shape));

  S3D(device_ref_put(s3d));
  S3D(shape_ref_put(shape));
  S3DUT(mesh_ref_put(mesh));

  *out_scene = scene;
}

int
main(int argc, char* argv[])
{
  struct s3d_scene* scene = NULL;
  unsigned long nsteps = 10000;
  double ks = 1.0;
  res_T res = RES_OK;
  int err = 0;

  /* Check command arguments */
  if(argc < 2 || argc > 4) {
    printf("Usage: %s OBJ_FILE [SAMPLES_COUNT [K_SCATTERING]]\n", argv[0]);
    goto error;
  }
#if 0 /*1:SPHERE ou 0:OBJET*/
   create_sphere(&scene);
#else
  /* Import file's content in the scene */
  res = import_obj(argv[1], &scene);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't import `%s'\n", argv[1]);
    goto error;
  }
#endif

  /* Set number of realizations */
  if(argc >= 3) {
    res = cstr_to_ulong(argv[2], &nsteps);
    if(nsteps <= 0 || res != RES_OK) {
      fprintf(stderr, "Invalid number of steps `%s'\n", argv[2]);
      goto error;
    }
  }

  /* Set ks */
  if(argc >= 4) {
    res = cstr_to_double(argv[3], &ks);
    if(res != RES_OK || ks <= 0) {
      fprintf(stderr, "Invalid k-scattering value `%s'\n", argv[3]);
      goto error;
    }
  }

  res = compute_4v_s(scene, nsteps, ks);
  if(res != RES_OK) {
    fprintf(stderr, "Error in 4V/S integration\n");
    goto error;
  }

exit:
  if(scene) S3D(scene_ref_put(scene));
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
      (unsigned long)mem_allocated_size());
    err = 1;
  }
  return err;
error:
  err = 1;
  goto exit;
}

